<?php
namespace {
    require('vendor/autoload.php');

    $router = F2::router();

    $router = new F2\SimpleRouter\SimpleRouter('routes');

    foreach (["/users", "/users/", "/users/123", "/users/123/", "/users/all-users"] as $path) {
        echo "* Routing $path:\n";
        try {
            $target = $router->resolve($path);
            echo "  - Found controller function\n";
            echo "  - running callback with arguments\n";
            echo "-----------------------------------------------------\n";
            call_user_func_array($target->getClosure(), $target->getParams());
            echo "-----------------------------------------------------\n";
        } catch (\F2\Common\Contracts\ExceptionInterface $e) {
            echo "  - HTTP status ".$e->getHttpStatusCode()."\n";
            echo "  - HTTP message ".$e->getHttpStatusMessage()."\n";
        }
        echo "\n";
    }
}
namespace routes {
    function users() {
        echo "Hello World from /users/ or \\routes\\users()\n";
    }
}
namespace routes\users {
    function index() {
        echo "Hello World from /users/ or \\routes\\users\\index()\n";
    }
    function _(int $id) {
        echo "Hello World, number $id from /users/$id/ or \\routes\\users\\_($id)\n";
    }
    function all_users() {
        echo "Hello World from /users/all-users/ or \\routes\\users\\all_users()\n";
    }
}
