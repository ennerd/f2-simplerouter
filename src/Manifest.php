<?php
declare(strict_types=1);

namespace F2\SimpleRouter;

use F2\Common\AbstractManifest;

class Manifest extends AbstractManifest {

    public function getF2Methods(): iterable {
        // Can't use service provider for this.
        yield "router"  => [ \F2\SimpleRouter\SimpleRouter::class, 'getInstance' ];
    }

    public function registerConfigDefaults(array $config): array {
        // Add the container as a default service
        $config['f2/simple-router']['root-namespace'] = 'routes';
        return $config;
    }

}
