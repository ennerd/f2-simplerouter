<?php

function vary(string $header) {
    header("Vary: ".$header, false);
}

function origins() {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Max-Age: 600");
    if($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
        if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"])) {
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); //Make sure you remove those you do not want
        }

        if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"])) {
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        }

        //Just exit with 200 OK with the above headers for OPTIONS method
        exit(0);
    }
}

/**
 * Allow the response to be publicly cached for a while
 */
function publicCache(int $seconds=31536000) {
    if (session_cache_expire() > $seconds) {
        session_cache_expire($seconds);
    }
    // Default is public cache, so no need
    simplerouter_update_headers();
}

/**
 * Allow the response to be cached by the browser for a while
 */
function privateCache(int $seconds=null) {
    if (session_cache_expire() > $seconds) {
        session_cache_expire($seconds);
    }
    if (session_cache_limiter() === 'public') {
        session_cache_limiter('private');
    }
    simplerouter_update_headers();
}

/**
 * Don't allow the response to be cached
 */
function noCache() {
    session_cache_expire(0);
    session_cache_limiter('nocache');
    simplerouter_update_headers();
}

function simplerouter_update_headers() {
    header("Date: ".gmdate('D, d M Y H:i:s', time())." GMT");
    switch (session_cache_limiter()) {
        case 'public' :
            header("Expires: ".gmdate('D, d M Y H:i:s', time() + session_cache_expire() - 1)." GMT");
            header("Cache-Control: public, max-age=".(session_cache_expire()));
            break;
        case 'private' :
            header("Expires: ".gmdate('D, d M Y H:i:s', time() + session_cache_expire() - 1)." GMT");
            header("Cache-Control: private, max-age=".(session_cache_expire()));
            break;
        case 'nocache' :
            header("Cache-Control: no-cache, no-store");
            break;
    }
//    header("Cache-Control: ".session_cache_limiter().", max-age=".session_cache_expire());
//    header("Last-Modified: ".gmdate('D, d M Y H:i:s', time())." GMT");
}
