<?php
declare(strict_types=1);

namespace F2\SimpleRouter;

use F2;
use F2\Common\Contracts\Router\RouterInterface;
use F2\Common\Contracts\Router\RouterResultInterface;

/**
 * Translates between functions and string paths according to a scheme, for routing purposes.
 */
class SimpleRouter implements RouterInterface {

    protected $root;

    /**
     * @param $root Root namespace
     */
    public function __construct(string $root) {
        $this->root = $root;
    }

    public static function getInstance(string $rootNamespace=null): RouterInterface {
        if ($rootNamespace === null) {
            $rootNamespace = F2::config("f2/simple-router root-namespace");
        }
        return new self($rootNamespace);
    }

    public function resolve(string $path): RouterResultInterface {
        $parts = explode("/", ltrim(urldecode($path), "/"));
        $result = static::resolveNamespacedFunction(rtrim($this->root, "\\")."\\", $parts);
        return new SimpleRouterResult($path, $result[0], $result[1]);
    }

    protected static function sanitize(string $part): string {
        return str_replace(["-"], ["_"], $part);
    }

    protected static function resolveNamespacedFunction(string $rootNamespace, array $parts): ?array {
        foreach ( static::generatePermutations($parts) as $candidate) {
            if (is_callable($fn = $rootNamespace.implode("\\", $candidate[0]))) {
                $params = $candidate[1];
                $paramCount = sizeof($params);
                $ref = new \ReflectionFunction($fn);
                if ($ref->getNumberOfRequiredParameters() > $paramCount) {
                    // The route did not provide enough parameters
                    continue;
                }
                $args = [];
                foreach ($ref->getParameters() as $param) {
                    $value = array_pop($params);
                    if (!$param->canBePassedByValue()) {
                        // The route requires a value to be passed by reference
                        continue 2;
                    }
                    if ($param->hasType()) {
                        // If the parameter is typed, then we validate type
                        $type = $param->getType();
                        switch ($param->getType()->getName()) {
                            case 'int':
                                if ($value !== (string) intval($value)) {
                                    // The part is not an integer
                                    continue 3;
                                }
                                $args[] = intval($value);
                                break;
                            case 'float':
                                if ($value != (string) floatval($value)) {
                                    // The part is not numeric
                                    continue 3;
                                }
                                $args[] = floatval($value);
                                break;
                            case 'bool':
                                if ($value === "true" || $value === "1") {
                                    $args[] = true;
                                } elseif ($value === "false" || $value === "0") {
                                    $args[] = false;
                                } else {
                                    // The parameter must be 0, 1, true or false
                                    continue 3;
                                }
                                break;
                            case 'string':
                                // Any string is acceptable
                                $args[] = $value;
                                break;
                            default:
                                die("Don't understand type ".$param->getType()->getName());
                        }
                    }
                }
                return [ $fn, array_merge($args, $params) ];
            }
        }
        return null;
    }

    protected static function resolveFunction(array $parts): ?array {
        // This function is left here, because it might be useful some day.
        // Translates /users/123/profile to users__profile(int $userid)
        // and /users/all to users_all()
        $prefix = 'routes\\';

var_dump($parts);

        $limit = pow(2, $count = sizeof($parts));
        for ($i = 0; $i < $limit; $i++) {
            $candidate = "";
            $map = str_pad(decbin($i), $count, "0", STR_PAD_LEFT);
echo "MAP: $map\n";
            for ($j = 0; $j < $count; $j++) {
                if ($map[$j] === "1") {
                    $candidate .= "*".($j === $count-1 ? "" : "_");
                } else {
                    $candidate .= $parts[$j].($j === $count-1 ? "" : "_");
                }
            }
echo "CANDIDATE: $candidate\n";
            if (trim($candidate, "_*") === "") {
                $candidate = str_replace("*", "", $candidate)."_";
            } else {
                if (substr($candidate, 0, 2) === "*_") {
                    $candidate = substr($candidate, 1);
                }
                if (substr($candidate, -2) === "_*") {
                    $candidate = substr($candidate, 0, -1);
                }
                $candidate = str_replace("_*", "_", $candidate);
            }
echo "$candidate\n\n";
        }
var_dump($limit);
die();

        return null;
    }

    protected static function resolveFile(string $result, array $parts, array $vars=[]): ?array {

        $next = array_shift($parts);
        if ($next === null) {
            if (file_exists($tmp = $result.'/index.php')) {
                return [ $tmp, $vars ];
            } elseif (file_exists($tmp = $result.'/_.php')) {
                $vars['_'] = "";
                return [ $tmp, $vars ];
            } else {
                return null;
            }
        }
        $candidate = $result.'/'.$next;
//        echo "result=$result next=$next vars=".json_encode($vars)."<br>\n";
        if (is_dir($candidate)) {
            return static::resolveFile($candidate, $parts, $vars);
        } elseif (file_exists($candidate.'.php')) {
            return [ $candidate.'.php', $vars ];
        } else {
            $candidates = glob($result.'/_*');
            if (sizeof($candidates) > 1) {
                throw new Exception("Multiple candidate routes is not supported in '$result' currently. Do we need it?");
            } elseif (isset($candidates[0])) {
                $name = substr(basename($candidates[0]), 1);
                if (is_dir($candidates[0])) {
                    $vars[$name] = $next;
                    return static::resolveFile($candidates[0], $parts, $vars);
                } elseif (substr($candidates[0], -4) === '.php') {
                    array_unshift($parts, $next);
                    $value = implode("/", $parts);
                    if ($name === '.php') {
                        $vars['_'] = $value;
                    } else {
                        $vars[substr($name, 0, -4)] = $value;
                    }
                    return [ $candidates[0], $vars ];
                }
            } else {
                return null;
            }
        }
    }


    /**
     * Generates all permutations of $parts, implemented as a generator
     * to enable early exit.
     */
    protected static function generatePermutations(array $parts): iterable {
        $count = sizeof($parts);
        if ($count === 0) {
            yield ["index"];
            return;
        }
        if ($parts[$count-1] !== "") {
            $count++;
            $parts[] = "";
        }

        $validIdentifier = function(string $label): bool {
            return preg_match('|^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$|', $label) > 0;
        };

        foreach ($parts as $k => $part) {
            $part = static::sanitize($part);
            if (preg_match('|^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$|', $part) > 0) {
                $fnParts[$k] = $part;
            } else {
                $fnParts[$k] = null;
            }
        }

        $limit = pow(2, $count);

        $emitted = [];

        for ($i = 0; $i < $limit; $i++) {
            $candidate = [];
            $vars = [];
            $map = str_pad(decbin($i), $count, "0", STR_PAD_LEFT);
            for ($j = 0; $j < $count; $j++) {
                if ($j === $count-1 && $parts[$j]==="") {
                    if ($map[$j] !== "0") {
                        $candidate[] = "index";
                    }
                } elseif ($map[$j] === "1") {
                    $candidate[] = "_";
                    $vars[] = $parts[$j];
                } else {
                    if ($fnParts[$j] === null) {
                        continue 2;
                    }
                    $candidate[] = $fnParts[$j];
                }
            }
            yield [ $candidate, $vars ];
            $emitted[] = [ $candidate, $vars ];
        }

        foreach ($emitted as $pair) {
            if (isset($pair[0][$count-1]) && $pair[0][$count-1] === "index") {
                continue;
            }

            while ("_" === ($top = array_pop($pair[0]))) {
                yield [ array_merge($pair[0], ["__"]), $pair[1] ];
            }
        }

    }
}
