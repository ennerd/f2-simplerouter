<?php
declare(strict_types=1);

namespace F2\SimpleRouter;

use F2;
use Closure;
use F2\Common\Contracts\Router\RouterResultInterface;

/**
 * Translates between functions and string paths according to a scheme, for routing purposes.
 */
class SimpleRouterResult implements RouterResultInterface {
    protected $path;
    protected $closure;
    protected $params;

    public function __construct(string $path, callable $callable, array $params) {
        $this->path = $path;
        $this->closure = Closure::fromCallable($callable);
        $this->params = $params;
    }

    public function getPath(): string {
        return $this->path;
    }

    public function getClosure(): Closure {
        return $this->closure;
    }

    public function getParams(): array {
        return $this->params;
    }
}
